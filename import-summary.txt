ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .gitignore
* README.md
* default.properties
* lib/
* lib/apache-mime4j-0.6.jar
* lib/gson-1.6.jar
* lib/httpclient-4.0.1.jar
* lib/httpcore-4.0.1.jar
* lib/httpmime-4.0.1.jar
* old_src/
* old_src/APICalls.java
* old_src/AuthenticateActivity.java
* old_src/CommentLayout.java
* old_src/CommentLinkView.java
* old_src/ContactsView.java
* old_src/FlickrFree.java
* old_src/GetCachedImageTask.java
* old_src/GlobalResources.java
* old_src/Groups.java
* old_src/ImageAdapter.java
* old_src/ImageCollections.java
* old_src/ImageComments.java
* old_src/ImageContext.java
* old_src/ImageFullScreen.java
* old_src/ImageGrid.java
* old_src/ImageInfo.java
* old_src/ImageSets.java
* old_src/ImageTags.java
* old_src/JSONParser.java
* old_src/JavaMD5Sum.java
* old_src/MultipartEntityMonitored.java
* old_src/PictureSendReceiver.java
* old_src/PictureSettings.java
* old_src/RestClient.java
* old_src/SearchView.java
* old_src/TransferProgress.java
* old_src/TransferService.java
* old_src/UserView.java
* tests/
* tests/default.properties
* tests/gen/
* tests/gen/com/
* tests/gen/com/kamosoft/
* tests/gen/com/kamosoft/flickr/
* tests/gen/com/kamosoft/flickr/R.java
* tests/proguard.cfg

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => flickr/src/main/AndroidManifest.xml
* res/ => flickr/src/main/res/
* src/ => flickr/src/main/java/
* tests/res/ => flickr/src/androidTest/res/
* tests/src/ => flickr/src/androidTest/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
