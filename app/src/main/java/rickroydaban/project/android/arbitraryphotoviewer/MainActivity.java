package rickroydaban.project.android.arbitraryphotoviewer;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import rickroydaban.project.android.arbitraryphotoviewer.customviews.CustomViewPager;
import rickroydaban.project.android.arbitraryphotoviewer.customviews.FragmentFacebookPhotos;
import rickroydaban.project.android.arbitraryphotoviewer.customviews.FragmentInstagramPhotos;
import rickroydaban.project.android.arbitraryphotoviewer.customviews.FragmentPinterestPhotos;

/**
 * Created by rickroydaban on 07/01/2017.
 */

public class MainActivity extends FragmentActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private ViewGroup tabInstagram, tabPinterest, tabFacebook, tabPrev;
    private CustomViewPager pager;
    private PagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pager = (CustomViewPager)findViewById(R.id.pager);
        adapter = new PagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(4);
        pager.addOnPageChangeListener(this);

        tabInstagram = (ViewGroup)findViewById(R.id.tabs_instagram);
        tabPinterest = (ViewGroup)findViewById(R.id.tabs_pinterest);
        tabFacebook = (ViewGroup)findViewById(R.id.tabs_facebook);

        tabInstagram.setOnClickListener(this);
        tabPinterest.setOnClickListener(this);
        tabFacebook.setOnClickListener(this);

        pager.post(new Runnable() {
            @Override
            public void run() {
                pager.setCurrentItem(1);
                pager.setCurrentItem(0);
            }
        });

        try {
            PackageInfo info = getPackageManager().getPackageInfo("rickroydaban.project.android.arbitraryphotoviewer", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void switchTab(ViewGroup tab){
        if(tabPrev != null){
            tabPrev.getChildAt(1).setVisibility(View.GONE);
        }

        tab.getChildAt(1).setVisibility(View.VISIBLE);
        tabPrev = tab;
    }


    @Override
    public void onPageSelected(int position) {
        switch (position){
            case 0: switchTab(tabInstagram); break;
            case 1: switchTab(tabPinterest); break;
            default: switchTab(tabFacebook); break;
        }
    }

    @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
    @Override public void onPageScrollStateChanged(int state) {}

    @Override
    public void onClick(View v) {
        if(v == tabInstagram){
            switchTab(tabInstagram);
            pager.setCurrentItem(0);
        }else if(v == tabPinterest){
            switchTab(tabPinterest);
            pager.setCurrentItem(1);
        }else if(v == tabFacebook){
            switchTab(tabFacebook);
            pager.setCurrentItem(2);
        }
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0: return FragmentInstagramPhotos.getInstance();
                case 1: return FragmentPinterestPhotos.getInstance();
                default: return FragmentFacebookPhotos.getInstance();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
