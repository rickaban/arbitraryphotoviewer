package rickroydaban.project.android.arbitraryphotoviewer.customviews;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.squareup.picasso.Picasso;

import net.londatiga.android.instagram.Instagram;
import net.londatiga.android.instagram.InstagramRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rickroydaban.project.android.arbitraryphotoviewer.InstagramAuthActivity;
import rickroydaban.project.android.arbitraryphotoviewer.R;
import rickroydaban.project.android.arbitraryphotoviewer.models.InstagramItem;

/**
 * Created by rickroydaban on 07/01/2017.
 */

public class FragmentInstagramPhotos extends Fragment implements ObservableScrollViewCallbacks, View.OnClickListener {
    private static FragmentInstagramPhotos instance;
    private final String DATA = "data";
    private final String IMAGES = "images";
    private final String LOW_RES = "low_resolution";
    private final String DEFAULT_RES = "standard_resolution";
    private final String URL = "url";
    private View buttonsAuth;
    private ObservableRecyclerView lv;
    private Adapter adapter;
    private List<InstagramItem> items;
    private Instagram instagram;
    private LoadingScreen loadingScreen;
    private Snackbar snackbar;

    public static FragmentInstagramPhotos getInstance(){
        if(instance == null)
            instance = new FragmentInstagramPhotos();
        return instance;
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String clientID = getResources().getString(R.string.instagram_client_id);
        String clientSecret = getResources().getString(R.string.instagram_client_secret);
        String registeredCallbackURL = getResources().getString(R.string.instagram_registered_callbackurl);
        instagram = new Instagram(getActivity(), clientID, clientSecret, registeredCallbackURL);

        final View v = inflater.inflate(R.layout.fragment_instaphotos, null);
        loadingScreen = new LoadingScreen(v);
        snackbar = new Snackbar(v);
        buttonsAuth = v.findViewById(R.id.buttons_auth);
        buttonsAuth.setOnClickListener(this);
        items = new ArrayList<>();
        lv = (ObservableRecyclerView)v.findViewById(R.id.lv);
        lv.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new Adapter();
        lv.setAdapter(adapter);
        lv.setScrollViewCallbacks(this);
        checkAuth();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkAuth();
    }

    private void checkAuth(){
        if(instagram.getSession().isActive()){
            buttonsAuth.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);

            InstagramRequest request = new InstagramRequest(instagram.getSession().getAccessToken());
            String endpointRecent = getResources().getString(R.string.instagram_endpoint_recentphotos);
            loadingScreen.startLoading();
            request.createRequest("GET", endpointRecent, null, new InstagramRequest.InstagramRequestListener() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONArray jItems = new JSONObject(response).getJSONArray(DATA);
                        List<InstagramItem> temp = new ArrayList<>();
                        for(int i=0; i<jItems.length(); i++){
                            JSONObject jItem = jItems.getJSONObject(i);
                            String thumbnailURL = jItem.getJSONObject(IMAGES).getJSONObject(LOW_RES).getString(URL);
                            String imgURL = jItem.getJSONObject(IMAGES).getJSONObject(DEFAULT_RES).getString(URL);
                            temp.add(new InstagramItem(imgURL, thumbnailURL));
                        }
                        items.clear();
                        items.addAll(temp);
                        adapter.notifyDataSetChanged();
                        loadingScreen.stopLoading();
                    } catch (JSONException e) {
                        loadingScreen.stopLoading();
                        snackbar.show(Snackbar.DisplayType.REJECT, e.getMessage(), 5);
                    }

                }

                @Override
                public void onError(String error) {
                    loadingScreen.stopLoading();
                    snackbar.show(Snackbar.DisplayType.REJECT, error, 5);
                }
            });
        }else{
            buttonsAuth.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    public void onClick(View v) {
        if(v == buttonsAuth){
            Intent intent = new Intent(getActivity(), InstagramAuthActivity.class);
            startActivity(intent);
        }
    }

    private class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView iv;
        private View cBg;

        public Holder(View itemView) {
            super(itemView);
            cBg = itemView.findViewById(R.id.cs_bg);
            iv = (ImageView) itemView.findViewById(R.id.iv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = lv.indexOfChild(v);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(items.get(pos).getImgUrl())));                }
            });
        }

        @Override
        public void onClick(View v) {
        }
    }

    private class Adapter extends RecyclerView.Adapter<Holder> {

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_image, parent, false));
        }

        @Override
        public void onBindViewHolder(final Holder holder, final int position) {
            final InstagramItem item = items.get(position);

            holder.iv.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(getActivity()).load(item.getThumbnailURL()).placeholder(R.drawable.icon_default).resize(holder.iv.getWidth(), holder.iv.getWidth()).centerCrop().into(holder.iv);
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }
}
