package rickroydaban.project.android.arbitraryphotoviewer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.text.SimpleDateFormat;

public class FacebookActivity extends Activity {
    private LoginButton btLogin;
    public CallbackManager fbCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);

        fbCallbackManager = CallbackManager.Factory.create();
        btLogin = (LoginButton)findViewById(R.id.login_button);
        btLogin.setReadPermissions("email", "public_profile", "user_photos");
        System.out.println("TESTX begin");
        btLogin.registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                final String userid = loginResult.getAccessToken().getUserId();
                System.out.println("TESTX userid "+userid);
                System.out.println("TESTX date "+new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss").format(loginResult.getAccessToken().getExpires()));
                System.out.println("TESTX success"+loginResult.getAccessToken());

                ((App)getApplication()).authHelper.fbLogin(userid);
                finish();
            }

            @Override
            public void onCancel() {
                System.out.println("TESTX cancelled ");
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println("TESTX logged in failed "+error);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
