package rickroydaban.project.android.arbitraryphotoviewer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import rickroydaban.project.android.arbitraryphotoviewer.App;

/**
 * Created by rickroydaban on 13/01/2017.
 */

public class AuthHelper {
    private final String KEY_PINTEREST_CLIENTID = "pinterestclientid";
    private final String KEY_FB_CLIENTID = "facebookid";
    private static AuthHelper instance;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public static AuthHelper getInstance(App app){
        if(instance == null)
            instance = new AuthHelper(app);
        return instance;
    }

    private AuthHelper(App app){
        prefs = PreferenceManager.getDefaultSharedPreferences(app);
        editor = prefs.edit();
    }

    public void pinterestLogin(String clientID){
        editor.putString(KEY_PINTEREST_CLIENTID, clientID).commit();
    }

    public void pinterestLogout(){
        editor.putString(KEY_PINTEREST_CLIENTID, null).commit();
    }

    public String getPinterestClientID(){
        return prefs.getString(KEY_PINTEREST_CLIENTID, null);
    }

    public void fbLogin(String userID){
        editor.putString(KEY_FB_CLIENTID, userID).commit();
    }

    public void fbLogout(){
        editor.putString(KEY_FB_CLIENTID, null).commit();
    }

    public String getFBUserID(){ return prefs.getString(KEY_FB_CLIENTID, null); }
}
