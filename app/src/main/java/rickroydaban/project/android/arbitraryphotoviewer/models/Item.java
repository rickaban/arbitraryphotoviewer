package rickroydaban.project.android.arbitraryphotoviewer.models;

/**
 * Created by rickroydaban on 13/01/2017.
 */

public class Item {
    protected String imgUrl;

    public Item(String imgUrl){
        this.imgUrl = imgUrl;
    }

    public String getImgUrl(){ return imgUrl; }
}
