package rickroydaban.project.android.arbitraryphotoviewer.customviews;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.pinterest.android.pdk.PDKCallback;
import com.pinterest.android.pdk.PDKClient;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rickroydaban.project.android.arbitraryphotoviewer.App;
import rickroydaban.project.android.arbitraryphotoviewer.PinterestAuthActivity;
import rickroydaban.project.android.arbitraryphotoviewer.R;
import rickroydaban.project.android.arbitraryphotoviewer.models.PinterestItem;

/**
 * Created by rickroydaban on 07/01/2017.
 */

public class FragmentPinterestPhotos extends Fragment implements ObservableScrollViewCallbacks, View.OnClickListener {
    private static FragmentPinterestPhotos instance;
    private ObservableRecyclerView lv;
    private Adapter adapter;
    private List<PinterestItem> items;
    private View buttonsAuth;
    private App app;
    private LoadingScreen loadingScreen;
    private Snackbar snackbar;

    public static FragmentPinterestPhotos getInstance(){
        if(instance == null)
            instance = new FragmentPinterestPhotos();
        return instance;
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_pinterestphotos, null);

        loadingScreen = new LoadingScreen(v);
        snackbar = new Snackbar(v);
        app = (App)getActivity().getApplication();
        items = new ArrayList<>();
        lv = (ObservableRecyclerView) v.findViewById(R.id.lv);
        lv.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new Adapter();
        lv.setAdapter(adapter);
        lv.setScrollViewCallbacks(this);

        buttonsAuth = v.findViewById(R.id.buttons_auth);
        buttonsAuth.setOnClickListener(this);

        checkAuth();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkAuth();
    }

    private void checkAuth(){
        if(app.authHelper.getPinterestClientID() == null){
            buttonsAuth.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }else{
            buttonsAuth.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            PDKClient.configureInstance(getActivity(), app.authHelper.getPinterestClientID());

            loadingScreen.startLoading();
            PDKClient.getInstance().getMyBoards("id,name,url", new PDKCallback(){

                @Override
                public void onResponse(JSONObject response) {
                    buttonsAuth.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);
                    try {
                        JSONArray boards = response.getJSONArray("data");
                        for(int i=0; i<boards.length(); i++){
                            PDKClient.getInstance().getBoardPins(boards.getJSONObject(i).getString("id"), "id,image", new PDKCallback(){
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        JSONArray pins = response.getJSONArray("data");
                                        List<PinterestItem> temp = new ArrayList<>();
                                        for(int i=0; i<pins.length(); i++){
                                            JSONObject pin = pins.getJSONObject(i);
                                            String name = "";
                                            String imgUrl = pin.getJSONObject("image").getJSONObject("original").getString("url");
                                            temp.add(new PinterestItem(imgUrl));
                                        }
                                        items.clear();
                                        items.addAll(temp);
                                        adapter.notifyDataSetChanged();
                                        loadingScreen.stopLoading();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        loadingScreen.stopLoading();
                                        snackbar.show(Snackbar.DisplayType.REJECT, e.getMessage(), 5);
                                    }
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadingScreen.stopLoading();
                        snackbar.show(Snackbar.DisplayType.REJECT, e.getMessage(), 5);
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if(v == buttonsAuth){
            Intent intent = new Intent(getActivity(), PinterestAuthActivity.class);
            startActivity(intent);
        }
    }

    private class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView iv;
        private View cBg;

        public Holder(View itemView) {
            super(itemView);
            cBg = itemView.findViewById(R.id.cs_bg);
            iv = (ImageView) itemView.findViewById(R.id.iv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = lv.indexOfChild(v);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(items.get(pos).getImgUrl())));                }
            });

        }

        @Override
        public void onClick(View v) {

        }
    }

    private class Adapter extends RecyclerView.Adapter<Holder> {

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_image, parent, false));
        }

        @Override
        public void onBindViewHolder(final Holder holder, final int position) {
            final PinterestItem item = items.get(position);

            holder.iv.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(getActivity()).load(item.getImgUrl()).placeholder(R.drawable.icon_default).resize(holder.iv.getWidth(), holder.iv.getWidth()).centerCrop().into(holder.iv);
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

    }

    @Override public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {}
    @Override public void onDownMotionEvent() {}
    @Override public void onUpOrCancelMotionEvent(ScrollState scrollState) {}

}
