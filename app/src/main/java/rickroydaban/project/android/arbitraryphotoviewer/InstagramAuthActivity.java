package rickroydaban.project.android.arbitraryphotoviewer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.londatiga.android.instagram.Instagram;
import net.londatiga.android.instagram.InstagramUser;

/**
 * Created by rickroydaban on 05/01/2017.
 */

public class InstagramAuthActivity extends Activity {
//    private WebView browser;
    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instaauth);

        app = (App)getApplication();
        Instagram instagram = new Instagram(this, "fa496000bb734cca8836f60a18231a58", "70f3f550876141d3b270b53ed5331037", "https://www.instagram.com/developer/register/");

        if(instagram.getSession().isActive()){
            System.out.println("TESTX alreaedy authenticated");
            finish();
        }else{
            instagram.authorize(new Instagram.InstagramAuthListener() {
                @Override
                public void onSuccess(InstagramUser user) {
                    finish();
                }

                @Override
                public void onError(String error) {
                    System.out.println("TESTX insta failed "+error);
                }

                @Override public void onCancel() {}
            });
        }
    }


}
