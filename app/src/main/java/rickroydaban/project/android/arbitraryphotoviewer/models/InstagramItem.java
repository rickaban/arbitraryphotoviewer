package rickroydaban.project.android.arbitraryphotoviewer.models;

/**
 * Created by rickroydaban on 13/01/2017.
 */

public class InstagramItem extends Item{
    private String thumbnailURL;

    public InstagramItem(String imgUrl){
        super(imgUrl);
//        super.imgUrl.replace("\\", "");
    }

    public InstagramItem(String imgUrl, String thumbnailURL){
        super(imgUrl);
//        super.imgUrl.replace("\\", "");
        this.thumbnailURL = thumbnailURL;
//        this.thumbnailURL.replace("\\", "");
    }

    public String getThumbnailURL(){ return thumbnailURL; }
}
