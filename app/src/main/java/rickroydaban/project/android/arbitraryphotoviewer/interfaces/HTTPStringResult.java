package rickroydaban.project.android.arbitraryphotoviewer.interfaces;

/**
 * Created by rickroydaban on 05/01/2017.
 */

public interface HTTPStringResult {
    void onSuccess(String result);
    void onFailure(String message);
}
