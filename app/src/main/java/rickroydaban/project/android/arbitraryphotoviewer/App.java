package rickroydaban.project.android.arbitraryphotoviewer;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import rickroydaban.project.android.arbitraryphotoviewer.utils.AuthHelper;

/**
 * Created by rickroydaban on 05/01/2017.
 */

public class App extends Application {
    public AuthHelper authHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);

        authHelper = AuthHelper.getInstance(this);
    }
}
