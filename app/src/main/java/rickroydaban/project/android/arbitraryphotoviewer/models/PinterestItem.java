package rickroydaban.project.android.arbitraryphotoviewer.models;

/**
 * Created by rickroydaban on 13/01/2017.
 */

public class PinterestItem extends Item{

    public PinterestItem(String imgUrl){
        super(imgUrl);
        super.imgUrl.replace("\\", "");
    }

    public String getImgUrl(){ return imgUrl; }
}
