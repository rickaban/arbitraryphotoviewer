package rickroydaban.project.android.arbitraryphotoviewer.customviews;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rickroydaban.project.android.arbitraryphotoviewer.App;
import rickroydaban.project.android.arbitraryphotoviewer.FacebookActivity;
import rickroydaban.project.android.arbitraryphotoviewer.R;
import rickroydaban.project.android.arbitraryphotoviewer.models.FBItem;

/**
 * Created by rickroydaban on 07/01/2017.
 */

public class FragmentFacebookPhotos extends Fragment implements ObservableScrollViewCallbacks, View.OnClickListener {
    private static FragmentFacebookPhotos instance;
    private final String DATA = "data";
    private final String NAME = "name";
    private final String PROFPIC = "Profile Pictures";
    private final String REDIRECT = "redirect";
    private final String FIELDS = "fields";
    private final String IMAGES = "images";
    private final String SOURCE = "source";

    private ObservableRecyclerView lv;
    private Adapter adapter;
    private List<FBItem> items;
    private View buttonsAuth;
    private App app;
    private LoadingScreen loadingScreen;
    private Snackbar snackbar;

    public static FragmentFacebookPhotos getInstance(){
        if(instance == null)
            instance = new FragmentFacebookPhotos();
        return instance;
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_fbphotos, null);

        loadingScreen = new LoadingScreen(v);
        snackbar = new Snackbar(v);
        app = (App)getActivity().getApplication();
        items = new ArrayList<>();
        lv = (ObservableRecyclerView) v.findViewById(R.id.lv);
        lv.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new Adapter();
        lv.setAdapter(adapter);
        lv.setScrollViewCallbacks(this);

        buttonsAuth = v.findViewById(R.id.buttons_auth);
        buttonsAuth.setOnClickListener(this);

        checkAuth();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkAuth();
    }

    private void checkAuth(){
        if(AccessToken.getCurrentAccessToken() == null || app.authHelper.getFBUserID() == null){
            buttonsAuth.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }else{
            buttonsAuth.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            loadingScreen.startLoading();
            new GraphRequest(
                AccessToken.getCurrentAccessToken(), "/"+app.authHelper.getFBUserID()+"/albums", null, HttpMethod.GET, new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            final JSONArray jAlbums = response.getJSONObject().getJSONArray(DATA);
                            long albumID = -1;
                            for(int i=0; i<jAlbums.length(); i++){
                                final JSONObject jAlbum = jAlbums.getJSONObject(i);
                                if(jAlbum.getString(NAME).equals(PROFPIC))
                                    albumID = jAlbum.getLong("id");
                            }

                            if(albumID > 0){
                                final long finalAlbumID = albumID;
                                Bundle params = new Bundle();
                                params.putBoolean(REDIRECT, false);
                                new GraphRequest(AccessToken.getCurrentAccessToken(), "/"+albumID+"/photos", params, HttpMethod.GET, new GraphRequest.Callback() {
                                    public void onCompleted(GraphResponse response) {
                                        try {
                                            final JSONArray jPhotos = response.getJSONObject().getJSONArray(DATA);

                                            items.clear();
                                            for(int i=0; i<jPhotos.length(); i++){
                                                final int pos = i;
                                                final long photoID = jPhotos.getJSONObject(i).getLong("id");
                                                Bundle b = new Bundle();
                                                b.putBoolean(REDIRECT, false);
                                                b.putString(FIELDS, IMAGES);

                                                new GraphRequest(AccessToken.getCurrentAccessToken(), "/"+photoID, b, HttpMethod.GET, new GraphRequest.Callback() {
                                                            public void onCompleted(GraphResponse response) {
                                                                try {
                                                                    JSONArray photos = response.getJSONObject().getJSONArray(IMAGES);
                                                                    if(photos.length() > 0){
                                                                        String imgURL = photos.getJSONObject(0).getString(SOURCE);
                                                                        String thumbnailURL = photos.getJSONObject(photos.length()-1).getString(SOURCE);
                                                                        items.add(new FBItem(imgURL, thumbnailURL));

                                                                        if(items.size() == jPhotos.length()) {
                                                                            adapter.notifyDataSetChanged();
                                                                            loadingScreen.stopLoading();
                                                                        }
                                                                    }
                                                                } catch (Exception e) {
                                                                    loadingScreen.stopLoading();
                                                                    snackbar.show(Snackbar.DisplayType.REJECT, e.getMessage(), 5);
                                                                }
                                                            }
                                                        }
                                                ).executeAsync();
                                            }
                                        } catch (Exception e) {
                                            loadingScreen.stopLoading();
                                            snackbar.show(Snackbar.DisplayType.REJECT, e.getMessage(), 5);
                                        }
                                    }
                                }).executeAsync();
                            }else{
                                loadingScreen.stopLoading();
                                snackbar.show(Snackbar.DisplayType.ACCEPT, "No photos yet", 5);
                            }
                        } catch (JSONException e) {
                            loadingScreen.stopLoading();
                            snackbar.show(Snackbar.DisplayType.REJECT, e.getMessage(), 5);
                        }
                    }
                }
            ).executeAsync();
        }
    }

    @Override
    public void onClick(View v) {
        if(v == buttonsAuth){
            Intent intent = new Intent(getActivity(), FacebookActivity.class);
            startActivity(intent);
        }
    }

    private class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private ImageView iv;
            private View cBg;

            public Holder(View itemView) {
                super(itemView);
                cBg = itemView.findViewById(R.id.cs_bg);
                iv = (ImageView) itemView.findViewById(R.id.iv);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = lv.indexOfChild(v);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(items.get(pos).getThumbnailURL())));
                    }

                });
            }

            @Override
            public void onClick(View v) {
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new Adapter.Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_image, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
            final Holder holder = (Holder)viewHolder;
            final FBItem item = items.get(position);

            holder.iv.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(getActivity()).load(item.getThumbnailURL()).placeholder(R.drawable.icon_default).resize(holder.iv.getWidth(), holder.iv.getWidth()).centerCrop().into(holder.iv);
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    @Override public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {}
    @Override public void onDownMotionEvent() {}
    @Override public void onUpOrCancelMotionEvent(ScrollState scrollState) {}

}
