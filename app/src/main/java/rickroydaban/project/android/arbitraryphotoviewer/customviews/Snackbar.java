package rickroydaban.project.android.arbitraryphotoviewer.customviews;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ValueAnimator;

import rickroydaban.project.android.arbitraryphotoviewer.R;

/**
 * Created by rickroydaban on 16/10/2016.
 */

public class Snackbar {
    private View cSnackbar, btDismiss;
    private TextView tvMessage;
    private int snackbarColor;

    public Snackbar(View v) {
        cSnackbar = v.findViewById(R.id.cs_snackbar);
        btDismiss = v.findViewById(R.id.tviews_snackbar_dismiss);
        tvMessage = (TextView) v.findViewById(R.id.tviews_snackbar_message);
        cSnackbar.setVisibility(View.GONE);

        btDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateClose();
            }
        });
    }

    public Snackbar(Activity activity){
        View v = activity.getWindow().getDecorView();
        cSnackbar = v.findViewById(R.id.cs_snackbar);
        btDismiss = v.findViewById(R.id.tviews_snackbar_dismiss);
        tvMessage = (TextView)v.findViewById(R.id.tviews_snackbar_message);
        cSnackbar.setVisibility(View.GONE);

        btDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            animateClose();
            }
        });
    }

    public void show(DisplayType type, String message, int seconds){
        cSnackbar.setVisibility(View.VISIBLE);
        switch (type){
            case NORMAL: snackbarColor = Color.parseColor("#AA000000"); break;
            case ACCEPT: snackbarColor = Color.parseColor("#AA227722"); break;
            case REJECT:
                snackbarColor = Color.parseColor("#EEE23D28");
                if(message.contains("timeout"))
                    message = "Failed to reach server";
                break;
        }

        cSnackbar.setBackgroundColor(snackbarColor);
        tvMessage.setText(message);
        if(seconds > 0){
            cSnackbar.postDelayed(new Runnable() {
                @Override
                public void run() {
                animateClose();
                }
            }, seconds*1000);
        }
    }

    public void dismiss(){
        animateClose();
    }

    public enum DisplayType{
        NORMAL,
        ACCEPT,
        REJECT;
    }

    private void animateClose(){
        ValueAnimator va = ValueAnimator.ofFloat(1, 0);
        va.setDuration(700);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float alpha = (float)animation.getAnimatedValue();
                cSnackbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, snackbarColor));
            }
        });
        va.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationEnd(Animator animation) {
                cSnackbar.setVisibility(View.GONE);
            }

            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
        va.start();
    }
}
