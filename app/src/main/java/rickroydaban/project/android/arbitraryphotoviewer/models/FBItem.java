package rickroydaban.project.android.arbitraryphotoviewer.models;

/**
 * Created by rickroydaban on 13/01/2017.
 */

public class FBItem extends Item {
    private String thumbnailURL;

    public FBItem(String imgUrl, String thumbnailURL) {
        super(imgUrl);
        this.thumbnailURL = thumbnailURL;
    }

    public String getThumbnailURL() { return thumbnailURL; }
}
