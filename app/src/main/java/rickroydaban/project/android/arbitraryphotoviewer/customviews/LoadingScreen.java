package rickroydaban.project.android.arbitraryphotoviewer.customviews;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.widget.ImageView;

import rickroydaban.project.android.arbitraryphotoviewer.R;

/**
 * Created by rickroydaban on 15/10/2016.
 */

public class LoadingScreen {
    private View loadingScreen;
    private ImageView ivLoad;

    public LoadingScreen(View v){
        loadingScreen = v.findViewById(R.id.cs_loading);
        ivLoad = (ImageView)v.findViewById(R.id.iviews_loading);
        loadingScreen.setVisibility(View.GONE);
    }

    public LoadingScreen(Activity activity){
        View v = activity.getWindow().getDecorView();
        loadingScreen = v.findViewById(R.id.cs_loading);
        ivLoad = (ImageView)v.findViewById(R.id.iviews_loading);
        loadingScreen.setVisibility(View.GONE);
    }

    public void startLoading(){
        loadingScreen.setVisibility(View.VISIBLE);
        ((AnimationDrawable)ivLoad.getDrawable()).start();
    }

    public void stopLoading(){
        ((AnimationDrawable)ivLoad.getDrawable()).stop();
        loadingScreen.setVisibility(View.GONE);
    }
}
